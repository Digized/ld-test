﻿using UnityEngine;
using System.Collections;

public class MaterialSpeed : MonoBehaviour {

    public Material mat;
	
	// Update is called once per frame
	void Update () {
	
        mat.SetTextureOffset("_MainTex",new Vector2(-Time.time,0));
	}
}
