﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IncreaseTime : MonoBehaviour {
    public Text highscoreText;
   public Text txt;
    public AudioSource sound;
  public  int score;
   public bool hasPlayed;
	// Use this for initialization
	void Start () {
        hasPlayed = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        score++;
        txt.text = "Score: " +score;

        if (score > PlayerPrefs.GetInt("HS"))
        {
            Debug.Log("SCORE");
            highscoreText.text = "NEW HIGH SCORE";
            if (!hasPlayed)
            {
                sound.Play();
                hasPlayed = true;
            }
        }
        else
        {
            hasPlayed = false;
        }
             
	}
}
