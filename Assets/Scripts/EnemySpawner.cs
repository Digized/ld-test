﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

   public float min, max;
  public  GameObject[] enemies;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Time.time % 10 == 0)
        {
            GameObject enemy = Instantiate(enemies[0], transform.position, Quaternion.identity) as GameObject;
            Rigidbody rb = enemy.GetComponent<Rigidbody>();
            rb.AddForce(-1000, Random.Range(min, max), Random.Range(min, max));
            ColorRandomizer(enemy);
           
        }
        if (Time.time % 7 == 0)
        {
			GameObject enemy =Instantiate(enemies[2],transform.position,Quaternion.identity) as GameObject;
			Rigidbody rb =enemy.GetComponent<Rigidbody>();
			rb.AddForce(-1000,0,0,ForceMode.Force);
			ColorRandomizer(enemy);
        }
        if (Time.time % 3 == 0)
        {
            GameObject enemy = Instantiate(enemies[1], transform.position, Quaternion.identity) as GameObject;
            enemy.transform.position = new Vector3(transform.position.x, Random.Range(-1.75f, 1.75f), Random.Range(-1.75f, 1.75f));
            Rigidbody rb = enemy.GetComponent<Rigidbody>();
            rb.AddForce(-1000, 0, 0,ForceMode.Acceleration);
            ColorRandomizer(enemy);
        }
    }
    void ColorRandomizer(GameObject enemy)
    {
		Color random = new Color (Random.Range (0, 255) / 255.0f, Random.Range (0, 255) / 255.0f, Random.Range (0, 255) / 255.0f);
       Renderer rend = enemy.GetComponent<Renderer>();
        rend.material.SetColor("_Color",random);
        rend.material.SetColor("_EmissionColor", random);
        Light l = enemy.GetComponent<Light>();
        l.color = random;
		    }
        
}