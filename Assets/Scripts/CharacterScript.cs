﻿using UnityEngine;
using System.Collections;

public class CharacterScript : MonoBehaviour {

    // Use this for initialization

    //speed to move at
    [Range(0,1)]
    public float speed;

	void Start () {
	
	}

	// Update is called once per frame
	void Update () {

        //basic controls WASD
        //W moves camera up
        if (Input.GetKey(KeyCode.W)){
            transform.position += Vector3.up*speed;
        }
        //S moves camera down
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * speed;
        }
        //D moves camera right
        if (Input.GetKey(KeyCode.D))
        {
            transform.position -= Vector3.forward * speed;
        }
        //A moves camera left
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.forward * speed;
        }
//
        if (transform.position.y > 1.2f)
        {
            transform.position = new Vector3(transform.position.x, 1.2f, transform.position.z);
        }
        if (transform.position.y < -1.2f)
        {
            transform.position = new Vector3(transform.position.x, -1.2f, transform.position.z);

        }
        if (transform.position.z > 0.59f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0.59f);
        }
        if (transform.position.z < -0.59f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -0.59f);

        }
    }
}
