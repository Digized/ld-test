﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Collision : MonoBehaviour {

    public Text txt;
    public GameObject controller;
    public AudioSource sound;
    public int lives;

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            lives--;
            Destroy(other.gameObject,0);
           
            sound.Play();
            if (lives <= 0)
            {
                
                gameOver();
            }
        }
    }
    void gameOver()
    {
        gameObject.GetComponent<CharacterScript>().enabled=false;
		controller.GetComponent<IncreaseTime> ().enabled = false;
        txt.text = "GAME OVER";
        controller.GetComponent<GameControl>().gameOver = true;
    }
}