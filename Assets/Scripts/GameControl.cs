﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public GameObject player;
    public bool gameOver;

  public  Text myText;
    public GameObject spawner;
    // Use this for initialization
    void Start()
    {
        gameOver = true;
        if (!PlayerPrefs.HasKey("HS"))
        {
            PlayerPrefs.SetInt("HS", 0);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
        if (gameOver)
        {
            if (gameObject.GetComponent<IncreaseTime>().score > PlayerPrefs.GetInt("HS")){
                PlayerPrefs.SetInt("HS", gameObject.GetComponent<IncreaseTime>().score);
            }

            gameObject.GetComponent<IncreaseTime>().txt.text = "High Score: " + PlayerPrefs.GetInt("HS");
            myText.text = "Press Spacebar to Play";
            player.GetComponent<CharacterScript>().enabled = false;
            spawner.GetComponent<EnemySpawner>().enabled = false;
            if (Input.GetKey(KeyCode.Space))
            {
               GameObject[] enemies= GameObject.FindGameObjectsWithTag("Enemy");
                foreach(GameObject enemy in enemies)
                {
                    Destroy(enemy);
                }

                gameObject.GetComponent<IncreaseTime>().enabled = true;
                gameObject.GetComponent<IncreaseTime>().score = 0;
                gameObject.GetComponent<IncreaseTime>().hasPlayed = false;
                player.GetComponent<CharacterScript>().enabled = true;
                myText.text = "";
                gameObject.GetComponent<IncreaseTime>().highscoreText.text = "";
                gameOver = false;
                player.GetComponent<Collision>().lives = 1;
                spawner.GetComponent<EnemySpawner>().enabled = true;
            }
            if ((Input.GetKey(KeyCode.LeftControl))&& (Input.GetKey(KeyCode.LeftShift))&& (Input.GetKey(KeyCode.J)))
            {
                PlayerPrefs.SetInt("HS", 0);
            }

        }

	}
}
